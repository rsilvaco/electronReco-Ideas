#!/bin/bash -l

if [ $# -lt 1 ] 
then
  echo "Usage: $0 <stage>"
  exit 1
fi

stage=$1
Simulations=('Bd2KstEE-11124001')
Years=('2012')
#nJobs=200
nJobs=104


if [[ $stage = "setup" ]]
then
  # Setup basic ROOT environment 
  . SetupProject.sh ROOT

  # Loop over all required configurations
  for sim in "${Simulations[@]}"
  do
    for year in "${Years[@]}"
    do 
      mkdir -p $sim/$year/Brunel/
      for i in `seq 1 ${nJobs}`
      do 
        mkdir -p $sim/$year/Brunel/Job_${i}
        root -l -q -b BrunelSetup.C"(\"${sim}\",\"${year}\",\"$i\")" 
      done
    done
  done
elif [[ $stage = "gen" ]]
then
  # Setup Gauss
  . SetupProject.sh Brunel v49r2 
  # loop submission
  for sim in "${Simulations[@]}"
  do
    for year in "${Years[@]}"
    do 
      for i in `seq 1 ${nJobs}`
      do 
        cd $sim/$year/Brunel/Job_${i}/ 
        qsub -l cput=05:00:00 submit.pbs
        cd -
      done
    done
  done
else
  echo "Please provide correct input values"
fi

