Electron reconstruction studies  
===============================

Baseline repository to share scripts related to the electron reconstruction in 
decays of the type B+ -> K+e+e- or B0 -> K*(892)e+e-. 

Method
------

In the following a list of steps are described for each stage of the XDST production. 
The process chain uses the sequence: 

Gauss (sim) --> Boole (xdigi) --> L0 (xdigi) --> Brunel (xdst) 

Notice that neither the HLT or the Stripping stages have been utilised. In the case of the HLT, I'm currently having issues of compatibility that in theory should be solved if necessary. 
Since I think this is not needed for our studies, I preferred to skip this part. These same holds for the Stripping (although here I now how to run over), but in this case I just decided not to apply any unnecessary cut. 
The bottom line is that the available xdst have the minimum requirement demanded by the detector. 

The XDST currently available use the `Bd_Kstee=DecProdCut.dec` configuration: 
```
Decay mode (11124001): B0 -> (K*0 -> K+ pi-) e+ e- BTOSLLBALL (Physics model)
```
In other words, you should see the same asymmetry that has been reported before.

Available samples
-----------------
SIM files (104):
```
/afs/cern.ch/user/r/rsilvaco/eos/lhcb/user/r/rsilvaco/RareDecays/forBiagio/SIM/*/file.sim
```
XDST files (104):
```
/afs/cern.ch/user/r/rsilvaco/eos/lhcb/user/r/rsilvaco/RareDecays/forBiagio/XDSTs/*/file.xdst
```
XDST generation
---------------

A detailed explanation of each stage is reported below. Note that since the system here in Zurich is mantained with Torch, I needed a bash script to produce the apropriate initial samples. One way to avoid this is to use Ganga instead. 

  - `Gauss`: initial part of the simulation (slow) which produces the sim files
      + [`genSetup.C`](genSetup.C) - C++ script tp generate the options described below. 
      + [`runGauss.sh`](runGauss.sh) - bash script that perform two of the options (user input): 
      `setup` for create directory and scripts; or `gen` to submit the generation. 
      Two scripts (of many subjobs) are produced with the first option, `Gauss-Job.py` and `submit.pbs`. An example of them is shown below: 

```python
#-- Gauss-Job.py script
from Gauss.Configuration import *

importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-nu2.5.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_LHEP_EmNoCuts.py")

GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 50110

#--Set database tags using those for Sim08
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20120831"
LHCbApp().CondDBtag = "sim-20121025-vc-mu100"

OutputStream("GaussTape").Output = "DATAFILE='PFN:file.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"

#--Number of events
nEvts = 2500
LHCbApp().EvtMax = nEvts
```

```vim
# submit.pbs script
. SetupProject.sh Gauss v49r1

mkdir -p /scratch/$USER/$PBS_JOBID
cp $PWD/Analysis/Studies/ElectronReco/Ntuples/Local/Bd2KstEE_XDST/Bd2KstEE-11124001/2012/Job_10/Gauss-Job.py /scratch/$USER/$PBS_JOBID/
cd /scratch/$USER/$PBS_JOBID

# Run gauss 
gaudirun.py Gauss-Job.py /home/hep/rsilvaco/cmtuser/Gauss_v49r1/Gen/DecFiles/options/11124001.py $LBPYTHIA8ROOT/options/Pythia8.py
# Copy output
mkdir -p /disk/users/rsilvaco/Bd2KstEE-11124001-2012/$PBS_JOBID/
cp -r . /disk/users/rsilvaco/Bd2KstEE-11124001-2012/$PBS_JOBID/.
cp -f /scratch/$USER/$PBS_JOBID/file.sim  /disk/users/rsilvaco/Bd2KstEE-11124001-2012/$PBS_JOBID/file.sim

# Clean up
rm -rf /scratch/$USER/$PBS_JOBID
```

  - `Boole`: digitization step of the XDST procedure (fast) that has as outcome a digi file
      + [`digiSetup.C`](digiSetup.C) - C++ script tp generate the options described below. 
      + [`runBoole.sh`](runBoole.sh) - similar to the Gauss stage, two steps are required in here. 
      For simplicity, only the `Boole-Job.py` script is shown below. In addition, in the `submit.pbs` 
      script the most relevant information is how to run `gaudirun.py Boole-Job.py`. 

```python
#-- Boole-Job.py script
from Boole.Configuration import *

importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/L0/L0TCK-0x0037.py")

#--Set database tags using those for Sim08
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20120831"
LHCbApp().CondDBtag = "sim-20121025-vc-mu100"

Boole().DatasetName = "file"
Boole().DigiType    = "Extended"
OutputStream("DigiWriter").Output = "DATAFILE='PFN:file.xdigi' TYP='POOL_ROOTTREE' OPT='RECREATE'"

#--Number of events
nEvts = 2500
LHCbApp().EvtMax = nEvts

from Gaudi.Configuration import *
EventSelector().Input = ["DATAFILE='PFN:file.sim' TYP='POOL_ROOTTREE' OPT='READ'"]
```
  - `L0 stage`: from the xdigi sample run the L0App (fast) - outcome is still a digi file
      + [`L0Setup.C`](L0Setup.C) - C++ script tp generate the options described below. 
      + [`runL0.sh`](runL0.sh) - similar to the Gauss stage, two steps are required in here. 
      For simplicity, only the `L0-Job.py` script is shown below. In addition, in the `submit.pbs` 
      script the most relevant information is how to run `gaudirun.py L0-Job.py`. 

```python
from Configurables import L0App

#--Set configurations for L0
L0App().TCK            = "0x0037"
L0App().outputFile     = "L0_file.xdigi"
L0App().ReplaceL0Banks = False
L0App().Simulation     = True
L0App().DDDBtag        = "dddb-20120831"
L0App().CondDBtag      = "sim-20121025-vc-mu100"
#--Number of events
nEvts = 2500
L0App().EvtMax         = nEvts

from GaudiConf import IOExtension
IOExtension().inputFiles(["file.xdigi"])
```

  - `Brunel`: from the xdigi sample run the reconstruction of the tracks - outcome is finally the xdst
      + [`BrunelSetup.C`](BrunelSetup.C) - C++ script tp generate the options described below. 
      + [`runBrunel.sh`](runBrunel.sh) - similar to the Gauss stage, two steps are required in here. 
      For simplicity, only the `Boole-Job.py` script is shown below. In addition, in the `submit.pbs` 
      script the most relevant information is how to run `gaudirun.py Brunel-Job.py`. 

```python
importOptions("$APPCONFIGOPTS/Brunel/DataType-2011.py")
importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")
importOptions("$APPCONFIGOPTS/Brunel/xdst.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

#--Set configurations for Brunel

Brunel().DataType   = "2012"
Brunel().Simulation = True
Brunel().InputType  = "DIGI"
Brunel().DigiType   = "Extended"
Brunel().OutputType = "XDST"
Brunel().DDDBtag    = "dddb-20120831"
Brunel().CondDBtag  = "sim-20121025-vc-mu100"
Brunel().WithMC     = True

#--Number of events
nEvts = 2500
Brunel().EvtMax         = nEvts

OutputStream("DstWriter").Output = "DATAFILE='PFN:file.xdst' TYP='POOL_ROOTTREE' OPT='REC'"

from Gaudi.Configuration import *
EventSelector().Input = ["DATAFILE='PFN:L0_file.xdigi' TYP='POOL_ROOTTREE' OPT='READ'"]
```