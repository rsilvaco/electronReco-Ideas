void BrunelSetup(TString Sim, TString Year, TString index){

  int LINE = atoi(index);
  std::ifstream f("/disk/users/rsilvaco/Bd2KstEE-11124001-2012/L0/listXDIGIFiles.dat");
  std::string s;

  for (int i = 1; i <= LINE; i++){
    std::getline(f, s);
  }

  TString nameFile = Sim+"/"+Year+"/Brunel/Job_"+index+"/Brunel-Job.py";
  std::ofstream myfile(nameFile);
  myfile << "" << std::endl;
  myfile << "from Brunel.Configuration import *" << std::endl;
  myfile << "" << std::endl;
  myfile << "importOptions(\"$APPCONFIGOPTS/Brunel/DataType-2011.py\")" << std::endl;
  myfile << "importOptions(\"$APPCONFIGOPTS/Brunel/MC-WithTruth.py\")" << std::endl;
  myfile << "importOptions(\"$APPCONFIGOPTS/Brunel/xdst.py\")" << std::endl;
  myfile << "importOptions(\"$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py\")" << std::endl;
  myfile << "" << std::endl;
  myfile << "#--Set configurations for Brunel" << std::endl;
  myfile << "" << std::endl;
  myfile << "Brunel().DataType   = \"2011\"" << std::endl;
  myfile << "Brunel().Simulation = True" << std::endl;
  myfile << "Brunel().InputType  = \"DIGI\"" << std::endl;
  myfile << "Brunel().DigiType   = \"Extended\"" << std::endl;
  myfile << "Brunel().OutputType = \"XDST\"" << std::endl;
  myfile << "Brunel().DDDBtag    = \"dddb-20120831\"" << std::endl;
  myfile << "Brunel().CondDBtag  = \"sim-20121025-vc-mu100\"" << std::endl;
  myfile << "Brunel().WithMC     = True" << std::endl;
  myfile << "" << std::endl;
  myfile << "#--Number of events" << std::endl;
  myfile << "nEvts = 2500" << std::endl;
  myfile << "Brunel().EvtMax         = nEvts" << std::endl;
  myfile << "" << std::endl;
  myfile << "OutputStream(\"DstWriter\").Output = \"DATAFILE='PFN:file.xdst' TYP='POOL_ROOTTREE' OPT='REC'\"" << std::endl;
  myfile << "" << std::endl;
  myfile << "from Gaudi.Configuration import *" << std::endl;
  myfile << "EventSelector().Input = [\"DATAFILE='PFN:L0_file.xdigi' TYP='POOL_ROOTTREE' OPT='READ'\"]" << std::endl;
  myfile << "" << std::endl;

  TString nameFileSub = Sim+"/"+Year+"/Brunel/Job_"+index+"/submit.pbs";
  std::ofstream myfileSub(nameFileSub);
  myfileSub << "# File script to submit Brunel jobs" << std::endl;
  myfileSub << "" << std::endl;
  myfileSub << ". SetupProject.sh Brunel v49r2 " << std::endl;
  myfileSub << "" << std::endl;
  myfileSub << "mkdir -p /scratch/$USER/$PBS_JOBID" << std::endl;
  myfileSub << "cp $PWD/Analysis/Studies/ElectronReco/Ntuples/Local/Bd2KstEE_XDST/"+Sim+"/"+Year+"/Brunel/Job_"+index+"/Brunel-Job.py /scratch/$USER/$PBS_JOBID/" << std::endl; 
  myfileSub << "cp /disk/users/rsilvaco/Bd2KstEE-11124001-2012/L0/"+TString(s)+" /scratch/$USER/$PBS_JOBID/" << std::endl; 
  myfileSub << "cd /scratch/$USER/$PBS_JOBID" << std::endl;
  myfileSub << "" << std::endl;
  myfileSub << "# Run Brunel " << std::endl;
  myfileSub << "gaudirun.py Brunel-Job.py" << std::endl;
  myfileSub << "# Copy output" << std::endl;
  myfileSub << "mkdir -p /disk/users/rsilvaco/"+Sim+"-"+Year+"/Brunel/$PBS_JOBID/" << std::endl;
  myfileSub << "cp -r . /disk/users/rsilvaco/"+Sim+"-"+Year+"/Brunel/$PBS_JOBID/." << std::endl;
  myfileSub << "cp -f /scratch/$USER/$PBS_JOBID/file.xdst  /disk/users/rsilvaco/"+Sim+"-"+Year+"/Brunel/$PBS_JOBID/file.xdst" << std::endl;
  myfileSub << "" << std::endl;
  myfileSub << "# Clean up" << std::endl;
  myfileSub << "rm -rf /scratch/$USER/$PBS_JOBID" << std::endl;
  myfileSub << "" << std::endl;

}
